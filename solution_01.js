// first solution with IIFE
/*  const fibonacci = (function() {
  const cache = {};

  return function(n){
    if (n == 0 || n == 1 ) {
      return n;
    }
    if (cache[n-1] === undefined) cache[n-1] = fibonacci(n-1);
    if (cache[n-2] === undefined) cache[n-2] = fibonacci(n-2);
    return cache[n-1] + cache[n-2];
  };
})(); */

// even better
const fibonacci = (function(){
  let cache = {};

  return function(n){
    if (n <= 1) {
      return n;
    }
    cache[n] = cache[n] || fibonacci(n-1) + fibonacci(n-2);
    return cache[n];
  }
})();