// second solution with memo function wrapper
function memo(func) {
  cache = {}
  return function (n) {
    if (!cache[n]) cache[n] = func(n);
    return cache[n];
  }
}

const fibonacci = memo((n) => {
  if (n == 0 || n == 1) {
    return n;
  }
  return fibonacci(n-1) + fibonacci(n-2);
})
// end of solution